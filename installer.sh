#!/bin/bash

    bash 0-preinstall.sh
    arch-chroot /mnt /root/archinstaller/1-setup.sh
    source /mnt/root/archinstaller/install.conf
    arch-chroot /mnt /usr/bin/runuser -u $username -- /home/$username/archinstaller/2-user.sh
    arch-chroot /mnt /root/archinstaller/3-post-setup.sh

#    echo "XFCE4 Themes werden jetzt heruntergeladen"
#    git clone https://gitlab.com/133732/xfce.git /mnt/home/$username/Downloads
#    chown -R $username:$username /mnt/home/$username
#    echo "Download abgeschlossen, bitte neustarten. Und das XFCE Backupscript ausführen."
