#!/usr/bin/env bash

echo -e "\nINSTALLING AUR SOFTWARE\n"

echo "CLONING: YAY"
cd ~
git clone "https://aur.archlinux.org/yay.git"
cd ${HOME}/yay
makepkg -si --noconfirm
cd ~

PKGS=(
'awesome-terminal-fonts'
'brave-bin' # Brave Browser
'dxvk-bin' # DXVK DirectX to Vulcan
'nerd-fonts-fira-code'
'ocs-url' # install packages from websites
'ttf-droid'
'ttf-hack'
'ttf-meslo' # Nerdfont package
'ttf-roboto'
'caffeine'
'mugshot'
'libinput-gestures'
)

for PKG in "${PKGS[@]}"; do
    yay -S --noconfirm $PKG
done

echo "XFCE4 Theme wird jetzt heruntergeladen"
git clone https://gitlab.com/133732/xfce.git $HOME/xfce/
cd $HOME/xfce
bash install.sh
echo "Theming abgeschlossen, bitte neustarten"

echo -e "\nDone!\n"
exit
